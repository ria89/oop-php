<?php
   require_once ('animal.php');
   require_once ('ape.php');
   require_once ('frog.php');

   $sheep = new Animal("shaun");

    echo "Name  : $sheep->name <br>"; // "shaun"
    echo "Legs  : $sheep->leg <br>"; // 4
    echo "cold blooded  : $sheep->cold_blooded <br>"; // "no"

    $sungokong = new Ape("Kera sakti");
    echo "<br>Name  : $sungokong->name <br>"; // "shaun"
    echo "Legs  : $sungokong->leg <br>"; // 4
    echo "cold blooded  : $sungokong->cold_blooded <br>"; // "no"
    $sungokong->yell();

    $kodok = new Frog("buduk");
    echo "<br><br>Name  : $kodok->name <br>"; // "shaun"
    echo "Legs  : $kodok->leg <br>"; // 4
    echo "cold blooded  : $kodok->cold_blooded <br>"; // "no"
    $kodok->jump();
    
?>